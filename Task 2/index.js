function superSort(value) {
    value = value.split(' ');
    let array = [];
    value.forEach(elem => {
        array.push({'origin' : elem, 'mod' : elem.replace('/[0-9]/g', '')});
    });
    array.sort((a, b) => a.mod > b.mod ? 1: -1);
    let result = [];

    array.forEach(elem => {
        result.push(elem.origin);
    });
    return result.join(" ");
}

function findNumdayser(array) {
    let current = array[0];
    for(let i = 1; i < array.length; i++) {
        if(current != array[i]) {
            return array[i];
        }
    }
}

function getDiscount(number) {
    return (number < 5) ? 1 : (number < 10 && number >= 5) ? 0.95 : 0.9;
}

function getDaysForMonth(month) {
    switch(month){
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            return 31;
            break;
        case 2: 
            return 28;
            break;
        case 4: 
        case 6:
        case 9:
        case 11:
            return 30;
            break;
        default: 
            return "Wrong month";
        }
}

function compareTwoDates(firstDate, secondDate) {
    firstDate = firstDate.split(/[.,/-]/g);
    secondDate = secondDate.split(/[.,/-]/g);
    
    if(firstDate[0].length===4&&secondDate[0].length===4) {
        return firstDate[0]===secondDate[0]
        &&firstDate[1]===secondDate[1]&&firstDate[2]===secondDate[2];
    }
    
    else if(firstDate[2].length===4&&secondDate[2].length===4) {
        return (firstDate[0]===secondDate[0]
        &&firstDate[1]===secondDate[1]&&firstDate[2]===secondDate[2])
        ||(firstDate[2]===secondDate[2]
            &&firstDate[1]===secondDate[0]&&firstDate[0]===secondDate[1]);
    }

    else if (firstDate[0].length===4&&secondDate[2].length===4||firstDate[2].length===4&&secondDate[0].length===4){
        return ((firstDate[0]===secondDate[1]&&firstDate[1]===secondDate[2]&&firstDate[2]===secondDate[0])
        ||(firstDate[2]===secondDate[0]&&firstDate[1]===secondDate[1]&&firstDate[0]===secondDate[2])
        ||(firstDate[0]===secondDate[2]&&firstDate[1]===secondDate[0]&&firstDate[2]===secondDate[1])
        );
    }

    else {
        return false;
    }
}

function popAllBubbles() {
    const elements = document.getElementsByClassName('bubble');
    let counter = 0;
    for(let i = 0; i < elements.length; i++) {
        elements[i].click();
        counter++;
    }
    console.log((counter).toString() === document.getElementById('score').innerText);
}
setTimeout(popAllBubbles, 5000);

let counter = 0;
function popAllBubblesForever() {
    let elements;
    elements  = document.getElementsByClassName('bubble');
    for(let i = 0; i < elements.length; i++) {
        elements[i].click();
        counter++;
       }
    console.log((counter).toString() === document.getElementById('score').innerText);
}
setTimeout(popAllBubblesForever, 5000);
setInterval(popAllBubblesForever, 0.001);