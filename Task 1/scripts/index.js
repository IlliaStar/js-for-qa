const API_URL = 'https://api.github.com/';
const rootElement = document.getElementById('root');
const loadingElement = document.getElementById('loading-overlay');
const fightersDetailsMap = new Map();

function callApi(endpoind, method) {
    const url = API_URL + endpoind
    const options = {
      method
    };
  
    return fetch(url, options)
        .then(response => 
            response.ok 
            ? response.json() 
            : Promise.reject(Error('Failed to load'))
        )
        .catch(error => { throw error });
}

class FighterService {
    async getFighters() {
      try {
        const endpoint = 'repos/sahanr/street-fighter/contents/fighters.json';
        const apiResult = await callApi(endpoint, 'GET');
        
        return JSON.parse(atob(apiResult.content));
      } catch (error) {
        throw error;
      }
    }

    async getDetailedInfoAboutFighter(id) {
      try {
        const endpoint = 'repositories/186232982/contents/resources/api/details/fighter/'+ id +'.json';
        const apiResult = await callApi(endpoint, 'GET');
        
        return JSON.parse(atob(apiResult.content));
      } catch (error) {
        throw error;
      }
    }
  }
  
const fighterService = new FighterService();


class View {
    element;
  
    createElement({ tagName, className = '', attributes = {} }) {
      const element = document.createElement(tagName);
      element.classList.add(className);
      
      Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));
  
      return element;
    }
}


class FighterView extends View {
    constructor(fighter, handleClick) {
      super();
  
      this.createFighter(fighter, handleClick);
    }
  
    createFighter(fighter, handleClick) {
      const { name, source } = fighter;
      const nameElement = this.createName(name);
      const imageElement = this.createImage(source);
  
      this.element = this.createElement({ tagName: 'div', className: 'fighter' });
      this.element.append(imageElement, nameElement);
      this.element.addEventListener('click', event => handleClick(event, fighter), false);
    }
  
    createName(name) {
      const nameElement = this.createElement({ tagName: 'span', className: 'name' });
      nameElement.innerText = name;
  
      return nameElement;
    }
  
    createImage(source) {
      const attributes = { src: source };
      const imgElement = this.createElement({
        tagName: 'img',
        className: 'fighter-image',
        attributes
      });
  
      return imgElement;
    }
  }
  
class FightersView extends View {
    constructor(fighters) {
      super();
      
      this.handleClick = this.handleFighterClick.bind(this);
      this.createFighters(fighters);
    }
  
    fightersDetailsMap = new Map();
  
    createFighters(fighters) {
      const fighterElements = fighters.map(fighter => {
        const fighterView = new FighterView(fighter, this.handleClick);
        return fighterView.element;
      });
  
      this.element = this.createElement({ tagName: 'div', className: 'fighters' });
      this.element.append(...fighterElements);
    }
  
    async handleFighterClick(event, fighter) {
      this.fightersDetailsMap.set(fighter._id, fighter);
      console.log('clicked');
      const info =await fighterService.getDetailedInfoAboutFighter(fighter._id);
      console.log(new Fighter(info));
      // get from map or load info and add to fightersMap
      // show modal with fighter info
      // allow to edit health and power in this modal
    }
}

class Fighter {

  constructor(detailedInfo) {
    this.id = detailedInfo._id;
    this.name = detailedInfo.name;
    this.health = detailedInfo.health;
    this.attack = detailedInfo.attack;
    this.defense = detailedInfo.defense;
    this.source = detailedInfo.source;
  }
  
  getBlockPower(min, max) {
    let criticalHitChance = Math.random() * (max - min) + min;
    return this.attack * criticalHitChance;
  }

  getHitPower(min, max) {
    let dodgeChance = Math.random() * (max - min) + min;
    return this.defense * dodgeChance;
  }
}

class App {
    constructor() {
      this.startApp();
    }
  
    static rootElement = document.getElementById('root');
    static loadingElement = document.getElementById('loading-overlay');
  
    async startApp() {
      try {
        App.loadingElement.style.visibility = 'visible';
        
        const fighters = await fighterService.getFighters();

        const figherInfo = await fighterService.getDetailedInfoAboutFighter(1); // gets detailed info about fighter by id - 1
        const firstFighterDetailedInfo = new Fighter(figherInfo); // creates fighter with id - 1
        console.log(firstFighterDetailedInfo);
        console.log(firstFighterDetailedInfo.getBlockPower(1, 2)); 
        console.log(firstFighterDetailedInfo.getHitPower(1, 2));

        const fightersView = new FightersView(fighters);
        const fightersElement = fightersView.element;
  
        App.rootElement.appendChild(fightersElement);
      } catch (error) {
        console.warn(error);
        App.rootElement.innerText = 'Failed to load data';
      } finally {
        App.loadingElement.style.visibility = 'hidden';
      }
    }
  }
  
new App();